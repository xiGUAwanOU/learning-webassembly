# learning-webassembly

Interesting readings:
* [Understanding the text format](https://developer.mozilla.org/en-US/docs/WebAssembly/Understanding_the_text_format)
* [Introduction to AssemblyScript](https://www.assemblyscript.org/introduction.html)
